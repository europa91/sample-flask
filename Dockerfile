FROM python:3.11-slim as build

ARG APP_FOLDER="/opt/app"

WORKDIR ${APP_FOLDER}
COPY ./src ${APP_FOLDER}

# Run vulnerability scan on build image
FROM build AS vulnscan
COPY --from=aquasec/trivy:latest /usr/local/bin/trivy /usr/local/bin/trivy
RUN trivy fs --no-progress /


# Run final image
FROM python:3.11-slim as final

ARG APP_FOLDER="/opt/app"

WORKDIR ${APP_FOLDER}
COPY --from=build ${APP_FOLDER} ${APP_FOLDER}

RUN addgroup --system --gid 1001 appgroup && \
    adduser --system --uid 1001 --gid 1001 --no-create-home operator && \
    chown -R 1001:1001 ${APP_FOLDER} && \
    pip3 install -r "${APP_FOLDER}/requirements.txt"

USER operator

# Set environment variables
ENV PIP_DEFAULT_TIMEOUT=100 \
    # Allow statements and log messages to immediately appear
    PYTHONUNBUFFERED=1 \
    # disable a pip version check to reduce run-time & log-spam
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    # cache is useless in docker image, so disable to reduce image size
    PIP_NO_CACHE_DIR=1 \
    # Set Time zone
    TZ="Europe/Paris" \
    # ENV dedicated to application
    MYCORP_NAME="My sample company"

EXPOSE 5000
ENTRYPOINT [ "python3", "${APP_FOLDER}/hello_stat.py" ]
