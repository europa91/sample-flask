#!/usr/bin/python3
import os
import socket
import datetime
from flask import Flask, redirect
try:
    from  redis import Redis, RedisError
except:
    pass


class Statcaching:

    def __init__(self, RedisObj=None):
        ''' Constructor '''
        self.RedisObj = RedisObj
        self.stat = {'hits': '0'}

    def getStat(self):
        ''' Getter stat structure
        @return: Dictionary structure '''
        self.refresh()
        return self.stat
    
    def refresh(self):
        ''' Refresh data with the cache '''
        try:
            if self.RedisObj:
                self.stat['hits'] = str(self.RedisObj.incr("hits"))
            else:
                self.stat['hits'] = '0'
        except RedisError:
            return "<i>Unkown value, sorry !</i>"


try:
    REDIS_SERVICE_HOST=os.getenv("REDIS_SERVICE_HOST")
    if not REDIS_SERVICE_HOST:
        REDIS_SERVICE_HOST = "redis"
except:
    REDIS_SERVICE_HOST = "redis"
try:
    REDIS_SERVICE_PORT=int(os.getenv("REDIS_SERVICE_PORT"))
    if not REDIS_SERVICE_PORT:
        REDIS_SERVICE_PORT = 6379
except:
    REDIS_SERVICE_PORT = 6379

REDIS_OBJ = Redis(host=REDIS_SERVICE_HOST, port=REDIS_SERVICE_PORT, db=0, socket_connect_timeout=2, socket_timeout=2)

app = Flask(__name__)

@app.route('/')
def index():
    ''' Route root '''
    return hello()

@app.route('/health')
def health():
    ''' Route health function '''
    return 'Ok'

@app.route('/hello')
def hello():
    ''' Route hello function '''
    stat = Statcaching(REDIS_OBJ).getStat()

    render = "<html><head><meta charset=\"utf-8\"></head><body><center><h2>Hello, Altran colleagues !</h2>" \
             "<p>Hostname container: {hostname}</p>" \
             "<p>Datetime: {now}</p>" \
             "<p>Hits: {hits}</p>" \
             "{user}</center></body></html>"
    
    return render.format(hostname=socket.gethostname(), user=os.getenv("MYCORP_NAME"), now=datetime.datetime.now(),hits=stat['hits'])


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0')
    
